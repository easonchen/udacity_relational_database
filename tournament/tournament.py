#!/usr/bin/env python
# 
# tournament.py -- implementation of a Swiss-system tournament
#

import psycopg2


def connect():
	"""Connect to the PostgreSQL database.  Returns a database connection."""
	return psycopg2.connect("dbname=tournament")


def deleteMatches():
	"""Remove all the match records from the database."""
	conn=connect()
	curs=conn.cursor()
	curs.execute("UPDATE standings SET wins=0, matches=0;")
	conn.commit()
	conn.close()


def deletePlayers():
	"""Remove all the player records from the database."""
	conn=connect()
	curs=conn.cursor()
	curs.execute('DELETE FROM standings;')
	conn.commit()
	curs.execute('DELETE FROM players;')
	conn.commit()
	conn.close()


def countPlayers():
	"""Returns the number of players currently registered."""
	conn=connect()
	curs=conn.cursor()
	curs.execute('SELECT count(*) FROM players;')
	count=curs.fetchall()
	conn.close()
	return count[0][0]


def registerPlayer(name):
	"""Adds a player to the tournament database.
  
    The database assigns a unique serial id number for the player.  (This
    should be handled by your SQL database schema, not in your Python code.)
  
    Args:
      name: the player's full name (need not be unique).
    """
	conn=connect()
	curs=conn.cursor()
	curs.execute("INSERT INTO players (name) VALUES ('{}');".format( name.replace("'","''")))
	conn.commit()
	curs.execute("SELECT id FROM players WHERE name='{}';".format( name.replace("'","''")))
	count=curs.fetchall()
	curs.execute("INSERT INTO standings (id, name, wins, matches) VALUES ({},'{}', 0, 0);".format( count[0][0], name.replace("'","''")))
	conn.commit()
	conn.close()


def playerStandings():
	"""Returns a list of the players and their win records, sorted by wins.

    The first entry in the list should be the player in first place, or a player
    tied for first place if there is currently a tie.

    Returns:
      A list of tuples, each of which contains (id, name, wins, matches):
        id: the player's unique id (assigned by the database)
        name: the player's full name (as registered)
        wins: the number of matches the player has won
        matches: the number of matches the player has played
    """
	conn=connect()
	curs=conn.cursor()
	curs.execute('SELECT * FROM standings ORDER BY wins;')
	list_standings=curs.fetchall()
	conn.close()
	return list_standings


def reportMatch(winner, loser):
	"""Records the outcome of a single match between two players.

    Args:
      winner:  the id number of the player who won
      loser:  the id number of the player who lost
    """
	conn=connect()
	curs=conn.cursor()
	curs.execute("UPDATE standings SET wins=wins+1, matches=matches+1 WHERE id={};".format(winner))
	conn.commit()
	curs.execute("UPDATE standings SET matches=matches+1 WHERE id={};".format(loser))
	conn.commit()
	conn.close()
 
 
def swissPairings():
	"""Returns a list of pairs of players for the next round of a match.
  
    Assuming that there are an even number of players registered, each player
    appears exactly once in the pairings.  Each player is paired with another
    player with an equal or nearly-equal win record, that is, a player adjacent
    to him or her in the standings.
  
    Returns:
      A list of tuples, each of which contains (id1, name1, id2, name2)
        id1: the first player's unique id
        name1: the first player's name
        id2: the second player's unique id
        name2: the second player's name
    """
	conn=connect()
	curs=conn.cursor()
	curs.execute("SELECT id, name FROM standings ORDER BY matches, wins DESC;")
	list_pairs=curs.fetchall()
	conn.close()
	return [list_pairs[2*i] + list_pairs[2*i+1] for i in range(len(list_pairs)/2)]
	
conn=connect()
curs=conn.cursor()
curs.execute("CREATE TABLE players (id SERIAL PRIMARY KEY, name TEXT);")
conn.commit()
curs.execute("CREATE TABLE standings(id INTEGER REFERENCES players(id), name TEXT, wins INTEGER, matches INTEGER);")
conn.commit()
conn.close()